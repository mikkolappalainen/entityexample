package com.example.EntityRestApi;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author mikko
 */
public class MainVerticle extends AbstractVerticle {

    private final Map<String, Entity> entities = new LinkedHashMap<>();

    @Override
    public void start(Future<Void> startFuture) throws Exception {
      //initialize the Vert.x router...  
      Router router = Router.router(vertx);
      router.route("/").handler(routingContext -> {
        HttpServerResponse response = routingContext.response();
        response
            .putHeader("content-type", "text/html")
            .end("<h1>Hello from my Entity exercise application</h1>");
      });
      //Create some demo data...  
      createSomeData();
      //Initialize the routes....

      router.route("/api/entities*").handler(BodyHandler.create());      
      router.get("/api/entities").handler(this::getEntities);
      router.post("/api/entities").handler(this::addEntity);
      router.put("/api/entities/:id").handler(this::updateEntity);
      router.get("/api/entities/:id").handler(this::getEntity);
      //initialize the HTTP server
      vertx.createHttpServer().requestHandler(router::accept)
      .listen(8888, http -> {
        if (http.succeeded()) {
          startFuture.complete();
          System.out.println("HTTP server started on port 8888");
        } else {
          System.out.println("HTTP server failed on port 8888");
          startFuture.fail(http.cause());
        }
      });
    }
    
    /**
    *
    * Create some fake data for testing
    * 
    */
    private void createSomeData() {
        Entity entity1 = new Entity();
        entity1.addOrUpdateData("1234", "Test value 1234 entity1");
        entity1.addOrUpdateData("12345", "Test value 12345 entity1");
        Entity entity2 = new Entity();
        entity2.addOrUpdateData("12345", "Test value 12345 entity2");
        Entity entity3 = new Entity();
        entity3.addOrUpdateData("1234", "Test value 1234 entity3");
        entity3.addOrUpdateData("12345", "Test value 12345 entity3");
        entity1.addSubEntity(entity2);
        entity1.addSubEntity(entity3);
        entities.put(entity1.getID(), entity1);

        Entity entity4 = new Entity();
        entity4.addOrUpdateData("1234", "Test value 1234 entity4");
        entity4.addOrUpdateData("12345", "Test value 12345 entity4");
        Entity entity5 = new Entity();
        entity5.addOrUpdateData("12345", "Test value 12345 entity5");
        Entity entity6 = new Entity();
        entity6.addOrUpdateData("1234", "Test value 1234 entity6");
        entity6.addOrUpdateData("12345", "Test value 12345 entity6");
        entity4.addSubEntity(entity5);
        entity4.addSubEntity(entity6);
        entities.put(entity4.getID(), entity4);
        //More fake data :)
        //entity2.addSubEntity(entity4);

    }    

    /**
    *
    * Writes all Entities from memory to HTTP stream
    * 
    * @see Entity
    */
    private void getEntities(RoutingContext routingContext) {
        System.out.println("getEntities called");
        System.out.println("getEntities sending "+Json.encodePrettily(entities.values()));
        routingContext.response()
            .putHeader("content-type", "application/json; charset=utf-8")
            .end(Json.encode(entities.values()));
    }

    /**
    *
    * Adds the given entity to memory
    * 
    * @see Entity
    */
    private void addEntity(RoutingContext routingContext) {
        try {
            System.out.println("addEntity called");
            System.out.println("addEntity got "+routingContext.getBodyAsString());
            JsonObject json = routingContext.getBodyAsJson();
            if (json == null) {
                routingContext.response().setStatusCode(400).end();
            } else {        
                final Entity entity = Json.decodeValue(routingContext.getBodyAsString(),
                    Entity.class);
                entities.put(entity.getID(), entity);
                System.out.println("addEntity sending "+Json.encodePrettily(entity));
                routingContext.response()
                    .setStatusCode(201)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encode(entity));
            }
        } catch (Exception e) {
            System.out.println("addEntity exception "+e.getMessage()+" "+Arrays.toString(e.getStackTrace()));
        }
    }
    
    /**
    *
    * Updates the given entity to memory. If the entity is subEntity then it is updated as such
    * 
    * @see Entity
    */
    private void updateEntity(RoutingContext routingContext) {
        try {
            System.out.println("updateEntity called");
            final String id = routingContext.request().getParam("id");
            System.out.println("updateEntity got "+routingContext.getBodyAsString());
            JsonObject json = routingContext.getBodyAsJson();
            if (id == null || json == null) {
                routingContext.response().setStatusCode(400).end();
            } else {        
                
                final Entity entity = Json.decodeValue(routingContext.getBodyAsString(),
                Entity.class);
                if (entity.getID()==null) {
                    routingContext.response().setStatusCode(404).end();
                    return;
                }

                if (entities.get(id)!=null) {
                    //If the ID changes - the ID parameter should be the old value...
                    if (!id.equals(entity.getID())) {
                        entities.remove(id);
                    }
                    entities.put(entity.getID(), entity);
                }
                else if (!updateSubEntity(id, entity)) {
                    routingContext.response().setStatusCode(404).end();
                }  
                System.out.println("updateEntity sending "+Json.encodePrettily(entity));
                routingContext.response()
                    .setStatusCode(201)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encode(entity));
            }
        } catch (Exception e) {
            System.out.println("updateEntity exception "+e.getMessage()+" "+Arrays.toString(e.getStackTrace()));
        }
    }

    /**
    *
    * Returns in HTTP stream the Entity with the given ID
    * 
    * @see Entity
    */
    private void getEntity(RoutingContext routingContext) {
        System.out.println("getEntity called");
        final String id = routingContext.request().getParam("id");
        if (id == null) {
          routingContext.response().setStatusCode(400).end();
        } else {
          Entity entity = entities.get(id);
          if (entity == null)
              entity = getSubEntity(id);
          
          if (entity == null) {
            routingContext.response().setStatusCode(404).end();
          } else {
            System.out.println("getEntity sending "+Json.encodePrettily(entity));
            routingContext.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encode(entity));
          }
        } 
    }
    
    /**
    *
    * Tries to find subEntity and updates it if found - loops all subEntries
    * 
    * @param id - the ID to look for
    * @param subEntityToUpdate - the Entity to update to memory
    * @param mainEntity - the Entity that is being search for it's subEntities
    * 
    * @return boolean - if update was successful then return true
    */
    private boolean updateSubEntity(String id, Entity subEntityToUpdate, Entity mainEntity) {
        for (EntityInterface subEntity : mainEntity.subEntities) {
            Entity entity = (Entity)subEntity;
            if (entity.getID().equals(id)) {
                mainEntity.subEntities.add(subEntityToUpdate);
                mainEntity.subEntities.remove(entity);
                return true;
            }
            if (entity.subEntities!=null) {
                if (updateSubEntity(id, subEntityToUpdate, entity)) return true;
            }
        }
        return false;
    }
    
    /**
    *
    * Tries to find subEntity and updates it if found
    * 
    * @param id - the ID to look for
    * @param subEntityToUpdate - the Entity to update to memory
    * 
    * @return boolean - if update was successful then return true
    */
    private boolean updateSubEntity(String id, Entity subEntityToUpdate) {
        /*    
            for (Entity entity:entities.values()) {
                if (entity.subEntities!=null) {
                    if (updateSubEntity(id, subEntityToUpdate, entity)) return true;
                }
            }
            return false;
        */ 
        return entities.values().stream().filter((entity) -> (entity.subEntities!=null)).anyMatch((entity) -> (updateSubEntity(id, subEntityToUpdate, entity)));
    }

    /**
    *
    * Tries to find subEntity 
    * 
    * @param id - the ID to look for
    * @param mainEntity - the Entity to search for subEntities
    * 
    * @return Entity 
    */
    private Entity getSubEntity(String id, Entity mainEntity) {
        for (EntityInterface subEntity : mainEntity.subEntities) {
            Entity entity = (Entity)subEntity;
            if (entity.getID().equals(id)) return entity;
            if (entity.subEntities!=null) {
                Entity subEntity2 = getSubEntity(id, entity);
                if (subEntity2!=null) return subEntity2;
            }
        }
        return null;
    }
    
    /**
    *
    * Tries to find subEntity from all root entities
    * 
    * @param id - the ID to look for
    * 
    * @return Entity 
    */
    private Entity getSubEntity(String id) {
        Entity subEntity = null;
        for (Entity entity:entities.values()) {
            if (entity.subEntities!=null)
                 subEntity = getSubEntity(id, entity);
            if (subEntity!=null) return subEntity;
        }
        return null;
    }

}
