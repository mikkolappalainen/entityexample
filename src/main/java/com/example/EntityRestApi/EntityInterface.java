/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EntityRestApi;

import java.util.Map;
import java.util.Set;

/**
 *
 * @author mikko
 */
public interface EntityInterface {
    // Returns a unique identifier
    String getID();
    // Returns the sub-entities of this entity
    Set<EntityInterface> getSubEntities();
    // Returns a set of key-value data belonging to this entity
    Map<String,String> getData();
}
