/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.EntityRestApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//So that extra fields are ignored 
@JsonIgnoreProperties(ignoreUnknown = true)
/**
 *
 * @author mikko
 */
public class Entity implements EntityInterface {
    
    public String id=null;
    public Set<EntityInterface> subEntities = null;
    public Map<String, String> data = null;
    
    public Entity() {
        id = java.util.UUID.randomUUID().toString();
    }
    
    /**
    *
    * UUID String 
    * 
    * @return String
    * 
    * @see UUID
    */
    @Override
    public String getID() {
//        if (sID==null) sID = java.util.UUID.randomUUID().toString();
        return id;
    }

    /**
    *
    * Adds the given sub entity to this object
    * 
    * @param entity - Entity to add to subEntities
    * 
    * @see Entity
    */
    public void addSubEntity(Entity entity) {
        if (subEntities==null)  subEntities = new HashSet<>();
        subEntities.add(entity);
    }

    /**
    *
    * Returns subEntities
    * 
    * @return Set<EntityInterface>
    * 
    * @see Entity
    */
    @Override
    public Set<EntityInterface> getSubEntities() {
        return subEntities;
    }

    /**
    *
    * Returns data as Map
    * 
    * @return Map<String, String>
    * 
    * @see Data
    */
    @Override
    public Map<String, String> getData() {
       return data;
    }

    /**
     * Adds new or updates data entry
     *
     * @param dataID - to identify data entry
     * @param dataStr - data string
     */
    public void addOrUpdateData(String dataID, String dataStr) {
        if (data==null)  data = new HashMap<>();
        data.put(dataID, dataStr);
    }
    
}
