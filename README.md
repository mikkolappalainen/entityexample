== Entity Example

Maven project in Neatbeans 11 with JDK 11 was used to develope the project.

A simple example on using Rest Api to:

list Entity entries
get http://localhost:8888/api/entities/

send an Entity (to root, or attached to another entity)
post http://localhost:8888/api/entities/
or put http://localhost:8888/api/entities/:id

read an Entity

http://localhost:8888/api/entities/:id


Entities are not saved so no persistance is offered.
SubEntities can be updated without updating parent. This hasn't been fully tested for performance.
No encryption is used nor is anykind of authentication.
Demo data is automatically created once the app is run. If it is not needed then comment out line MainVerticle's line 34.


== Building

To launch your tests:
```
./mvnw clean test
```

To package your application:
```
./mvnw clean package
```

To run your application:
```
./mvnw clean compile exec:java
```

== Help

Currently there is no more help :)

